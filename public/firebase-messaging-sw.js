importScripts('https://www.gstatic.com/firebasejs/6.4.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.4.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
    apiKey: "AIzaSyD10-JvpnC3kLsIF0Ettc_UKFmoGLrDi2g",
    authDomain: "pusher-74824.firebaseapp.com",
    databaseURL: "https://pusher-74824.firebaseio.com",
    projectId: "pusher-74824",
    storageBucket: "",
    messagingSenderId: "603468760965",
    appId: "1:603468760965:web:0dfc5306a1efa1db"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: 'Background Message body.',
        icon: '/itwonders-web-logo.png'
    };

    return self.registration.showNotification(notificationTitle,
            notificationOptions);
});