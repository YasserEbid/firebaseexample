<html>
    <title>Firebase Messaging Demo</title>
    <style>
        div {
            margin-bottom: 15px;
        }
    </style>
    <body>
        <div id="token"></div>
        <div id="msg"></div>
        <div id="notis"></div>
        <div id="err"></div>
        <button onclick="sendNotification()">Send Notification</button>





        <script src="<?= url('js/jquery.js') ?>"></script>
        <!--Insert these scripts at the bottom of the HTML, but before you use any Firebase services--> 

        <!--Firebase App (the core Firebase SDK) is always required and must be listed first--> 
        <script src="https://www.gstatic.com/firebasejs/6.4.0/firebase-app.js"></script>

        <!--Add Firebase products that you want to use--> 
        <script src="https://www.gstatic.com/firebasejs/6.4.0/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/6.4.0/firebase-firestore.js"></script>
        <script src="https://www.gstatic.com/firebasejs/6.4.0/firebase-messaging.js"></script>
        <script>
            MsgElem = document.getElementById("msg");
            TokenElem = document.getElementById("token");
            NotisElem = document.getElementById("notis");
            ErrElem = document.getElementById("err");
            const firebaseConfig = {
                apiKey: "AIzaSyD10-JvpnC3kLsIF0Ettc_UKFmoGLrDi2g",
                authDomain: "pusher-74824.firebaseapp.com",
                databaseURL: "https://pusher-74824.firebaseio.com",
                projectId: "pusher-74824",
                storageBucket: "",
                messagingSenderId: "603468760965",
                appId: "1:603468760965:web:0dfc5306a1efa1db"
            };
            firebase.initializeApp(firebaseConfig);

            //starting 
            const messaging = firebase.messaging();
            messaging
                    .requestPermission()
                    .then(function () {
                        MsgElem.innerHTML = "Notification permission granted."
                        console.log("Notification permission granted.");

                        // get the token in the form of promise
                        return messaging.getToken();
                    })
                    .then(function (token) {
                        // print the token on the HTML page
                        TokenElem.innerHTML = "token is : " + token
                        $.ajax({
                            url: '<?= url('save_token') ?>',
                            method: 'post',
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "browser_token": token
                            }
                        });
                    })
                    .catch(function (err) {
                        ErrElem.innerHTML = ErrElem.innerHTML + "; " + err
                        console.log("Unable to get permission to notify.", err);
                    });
            messaging.onMessage(function (payload) {
                console.log("Message received. ", payload);
                NotisElem.innerHTML = NotisElem.innerHTML + JSON.stringify(payload)
            });

            function sendNotification() {
                $.ajax({
                    url: '<?= url('send_notification') ?>',
                    method: 'post',
                    data: {
                        "_token": "{{ csrf_token() }}"
                    }
                });
            }
        </script>
    </body>
</html>