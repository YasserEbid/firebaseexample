<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrowsersTokens extends BaseModel
{

    protected $table = 'browsers_tokens';
    public $timestamps = true;
    public $rules = [
        'browser_token' => 'required|unique:browsers_tokens'
    ];
    protected $guarded = ['id'];

}
