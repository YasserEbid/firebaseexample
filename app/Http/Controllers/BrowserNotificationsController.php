<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BrowsersTokens;

class BrowserNotificationsController extends Controller {

    public function saveToken(Request $request) {
        if ($request->ajax()) {
            $browserToken = new BrowsersTokens();
            if ($browserToken->validate($request->all())) {
                $browserToken->browser_token = $request->browser_token;
                $browserToken->save();
            } else {
                dd($browserToken->errors());
            }
        }
    }

    function sendNotification(Request $request) {
//        if ($request->has('send')) {
//            $icon = \Mediasci\Cms\Models\Media::find($request->icon);
//            if (is_object($icon))
//                $icon = url('uploads/' . $icon->media_path);
//            else
//                $icon = '';
            $request->request->set('title', "Testing Notification");
            $request->request->set('body', "This is the body of the notification");
            $request->request->set('icon', '');
//            $request->request->remove('send');
//            $request->request->remove('site_id');

            $browsersToken = BrowsersTokens::orderBy('id', 'DESC')->get();
            foreach ($browsersToken as $token) {
                $key = 'key=AAAAjIGKh4U:APA91bFE637zBiRn_sUlMTx9jGRB1wQAS_gS02OPvGkNH_y8KGubenmZjjvDGnrqvckn5M2VxyOjMXKnsfIFU_D5q61mHxwIkgMaE7_q8WqR3YmvuByzNEjVvA2PX4ivxawI_chywIxi';

                $notification = [
                    'to' => $token->browser_token,
                    'notification' => $request->all()
                ];
                $notification_json = json_encode($notification);
                $fcm_url = 'https://fcm.googleapis.com/fcm/send';
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $fcm_url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Authorization: ' . $key)
                );
                curl_setopt($ch, CURLOPT_POSTFIELDS, $notification_json);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 2);
                $x = curl_exec($ch);
                $x = curl_error($ch);
            }

//        }

    }

}
